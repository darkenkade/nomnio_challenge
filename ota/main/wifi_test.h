#ifndef _WIFI_TEST_H_
#define _WIFI_TEST_H_

#define WIFI_SSID CONFIG_WIFI_SSID
#define WIFI_PASS CONFIG_WIFI_PASS

void wifi_init_station(void);
void wifi_wait_until_connected(void);

#endif
  
