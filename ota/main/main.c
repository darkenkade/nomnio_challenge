
#include <string.h>
#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "cJSON.h"
#include "esp_system.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"

#include "wifi_test.h"

#define VERSION 1
#define UPDATE_URL CONFIG_UPDATE_URL
#define BLINK_GPIO9 13
#define BLINK_GPIO3 3
#define BLINK_GPIO5 5

extern const uint8_t server_cert_pem_start[] asm("_binary_ca_cert_pem_start");
extern const uint8_t server_cert_pem_end[] asm("_binary_ca_cert_pem_end");

char buffer[100];
esp_err_t _http_event_handle(esp_http_client_event_t *event){
  switch(event->event_id){
  case HTTP_EVENT_ERROR:
    break;
  case HTTP_EVENT_ON_CONNECTED:
    break;
  case HTTP_EVENT_HEADER_SENT:
    break;
  case HTTP_EVENT_ON_HEADER:
    break;
  case HTTP_EVENT_ON_DATA:
    printf("HTTP_EVENT_ON_DATA, len=%d\n",event->data_len);
    if(!esp_http_client_is_chunked_response(event->client)){
      strncpy(buffer,(char *)event->data,event->data_len);
    }
    break;
  case HTTP_EVENT_ON_FINISH:
    break;
  case HTTP_EVENT_DISCONNECTED:
    break;
  }
  return ESP_OK;
}

void update_task(void *pvParameter){
  printf("Update task\n");
  for(;;){
    printf("Checking for updates...\n");
    esp_http_client_config_t config={
				     .url = UPDATE_URL,
				     .event_handler=_http_event_handle
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);
    if(err == ESP_OK){
      printf("%s\n",buffer);
      cJSON *json = cJSON_Parse(buffer);
      cJSON *version = cJSON_GetObjectItemCaseSensitive(json,"version");
      cJSON *url = cJSON_GetObjectItemCaseSensitive(json,"url");
      if(version->valueint>VERSION){
	vTaskDelay(10000/portTICK_PERIOD_MS);
	esp_http_client_config_t ota_config = {
					       .url=url->valuestring,
					       .cert_pem = (char *)server_cert_pem_start
	};
	esp_err_t ret = esp_https_ota(&ota_config);
	if(ret == ESP_OK){
	  printf("OTA GOOD TO GO, RESTARTING...\n");
	  esp_restart(); // END
	}
	else{
	  printf("OTA FAILED...\n");
	}
      }
    }
    vTaskDelay(10000/portTICK_PERIOD_MS);
  }
}

void blink_leds_task(void *pvParameter){
  gpio_pad_select_gpio(BLINK_GPIO9);
  gpio_pad_select_gpio(BLINK_GPIO3);
  gpio_pad_select_gpio(BLINK_GPIO5);
  
  gpio_set_direction(BLINK_GPIO9, GPIO_MODE_OUTPUT);
  gpio_set_direction(BLINK_GPIO3, GPIO_MODE_OUTPUT);
  gpio_set_direction(BLINK_GPIO5, GPIO_MODE_OUTPUT);
  
  gpio_set_level(BLINK_GPIO9,1);
  gpio_set_level(BLINK_GPIO3,0);
  gpio_set_level(BLINK_GPIO5,1);
  
  while(1){
    vTaskDelay(500/ portTICK_PERIOD_MS);
    
    gpio_set_level(BLINK_GPIO9,1);
    gpio_set_level(BLINK_GPIO3,0);
    gpio_set_level(BLINK_GPIO5,1);
    
    vTaskDelay(500/ portTICK_PERIOD_MS);
    gpio_set_level(BLINK_GPIO9,0);
    gpio_set_level(BLINK_GPIO3,1);
    gpio_set_level(BLINK_GPIO5,0);
  }
}
  

void app_main(void)
{
    xTaskCreate(&blink_leds_task,"blink",2048,NULL,5,NULL);
    wifi_init_station();
    printf("Wifi init\n");
    wifi_wait_until_connected();
    printf("Connected\n");
    xTaskCreate(&update_task,"update",9116,NULL,5,NULL);
}
