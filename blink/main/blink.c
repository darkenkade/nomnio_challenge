#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "sdkconfig.h"


#define BLINK_GPIO 13
#define BLINK_GPIO3 3
#define BLINK_GPIO5 5
#define LENGTH 3

uint32_t delay;
uint32_t repeats;
int pin_arr[]={BLINK_GPIO,BLINK_GPIO3,BLINK_GPIO5};

void st_lvl(int pin,int bit){
  gpio_set_level(pin,bit);
}

void delay_ms(int ms){
  vTaskDelay(ms / portTICK_PERIOD_MS);
}

void fill(int bit){
  for(int i=0;i<LENGTH;++i){
    st_lvl(pin_arr[i],bit);
  }
}

void turn_on_all(void){
  fill(1);
}

void clear_all(void){
  fill(0);
}

void sprehod(void){
  for(int s = 0;s<repeats;++s){
    for(int i = 0 ;i<LENGTH;++i){
      clear_all();
      st_lvl(pin_arr[i],1);
      delay_ms(delay);
    }
  }
}

void sprehod_nazaj(void){
  for(int s = 0;s<repeats;++s){
    for(int i = LENGTH-1 ;i>-1;--i){
      clear_all();
      st_lvl(pin_arr[i],1);
      delay_ms(delay);
    }
  }
}

void train(void){
  clear_all();
  for(int j=0;j<repeats;++j){
    for(int i=0;i<LENGTH;++i){
      st_lvl(pin_arr[i],1);
      delay_ms(delay);
    }
    for(int i=0;i<LENGTH;++i){
      st_lvl(pin_arr[i],0);
      delay_ms(delay);
    }
  }
}


void app_main(void)
{

  for(int i = 0;i<LENGTH;++i){
    gpio_pad_select_gpio(pin_arr[i]);
    gpio_set_direction(pin_arr[i], GPIO_MODE_OUTPUT);
    gpio_set_level(pin_arr[i],0);
  }
  while(1){
    int choice = esp_random() % 3;
    delay = esp_random() % 400 + 100;
    repeats = esp_random() % 5 + 1;
    switch(choice){
    case 0:
      sprehod();
      break;
    case 1:
      sprehod_nazaj();
      break;
    case 2:
      train();
      break;
    }
  }
}
